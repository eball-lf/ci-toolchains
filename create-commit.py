"""
Create a PR using the Github Python SDK

There are three places this PR can come from:
    1. From a branch in the Github repository [local_repo]
    2. From a fork in the automation user's account (GITHUB_USER) [remote_user_repo]
    3. From a fork in the automation organization's (GITHUB_ORG) [remote_org_repo]
"""
import sys
import shutil
import time

from os import getcwd, getenv, scandir, path, mkdir
from pathlib import Path
from tempfile import mkdtemp
from git import Repo
from github import Github


def getenv_orfail(envvar):
    """Get the environment variable or fail if its not set"""
    var = getenv(envvar)
    if not var:
        sys.exit("Please set %s" % var)
    return var


# Repo that pull request will be made on.
GITHUB_REPO = getenv_orfail('GITHUB_REPOSITORY')

# Github organization that forks will be created in. If unset, forks
# will be created in the GITHUB_USERNAME account space.
GITHUB_ORG = getenv('GITHUB_ORG')
# Github user that write access to current/new repos in that org.
# GITHUB_USERNAME = getenv('GITHUB_USERNAME')
# Github token for GITHUB_USERNAME
GITHUB_TOKEN = getenv_orfail('GITHUB_TOKEN')

PR_LOCAL_REPO = 'local_repo'
PR_REMOTE_USER_REPO = 'remote_user_repo'
PR_REMOTE_ORG_REPO = 'remote_org_repo'
# Defines the method for submitting the PR - See module doc TODO
GITHUB_PR_METHOD = getenv('GITHUB_PR_METHOD', PR_LOCAL_REPO)
GITHUB_PR_METHODS = [PR_LOCAL_REPO, PR_REMOTE_USER_REPO, PR_REMOTE_ORG_REPO]

if len(sys.argv) < 2:
    sys.exit("Please specify the directory to commit")

PATH = sys.argv[1]

TMP_DIR = None
RUNNING_IN_CI = getenv("CI", False)

# If running on Gitlab, create the directory where the build has access
# to write.
if RUNNING_IN_CI:
    TMP_DIR = getenv("CI_BUILDS_DIR", getcwd())

# Path where forked repo will be checkedout, and new files will be staged.
TMP_PATH = mkdtemp(dir=TMP_DIR)

COMMIT_HEADER = "New Commit"
COMMIT_BODY = "Adds CI"
COMMIT_FOOTER = "Signed-off-by: ITX CI <itx@linuxfoundation.org>"
COMMIT_MSG = "\n\n".join([COMMIT_HEADER, COMMIT_BODY, COMMIT_FOOTER])

AUTHOR = "ITX CI"
AUTHOR_EMAIL = "itx@linuxfoundation.org"
COMMITTER = AUTHOR
COMMITTER_EMAIL = AUTHOR_EMAIL

PR_TITLE = "New CI"
# on-behalf-of: @linuxfoundation-it <itx@linuxfoundation.org>
PR_HEADER = "Add new CI please!"
# Pull PR text body in from the file PULL_REQUEST_TEXT, then remove the
# file so it does not get committed.
_PR_FILE = Path(PATH).joinpath("PULL_REQUEST_TEXT")
PR_BODY = _PR_FILE.read_text()
_PR_FILE.unlink()
PR_FOOTER = "Signed-off-by: %s \<%s\>\n" % (AUTHOR, AUTHOR_EMAIL)  # noqa
PR_FOOTER += "on-behalf-of: @linuxfoundation-it \<%s\>" % AUTHOR_EMAIL
PR_MSG = "\n".join([PR_HEADER, PR_BODY, PR_FOOTER])
PR_BRANCH = "ci-%s" % int(time.time())


def copy(src, dst):
    """Copy all the files from one directory to another"""
    for item in scandir(src):
        if path.isfile(item):
            shutil.copy(item, dst)

        elif path.isdir(item):
            new_dst = path.join(dst, item.name)
            if not path.exists(new_dst):
                mkdir(new_dst)
            copy(item, new_dst)


def main():
    """Create a PR against GITHUB_REPOSITORY

    GITHUB_ORG/GITHUB_REPOSITORY
      refers to the organization/repository the PR is against

    This script:
     1. Forks the GITHUB_REPOSITORY to according to the
        GITHUB_PR_METHOD:
          local_repo - Branch is created in the GITHUB_REPOSITORY. No
              fork is created
          remote_user_repo - Fork is created in the GITHUB_USER account
          remote_org_repo - Fork is created in the GITHUB_ORG account
     2. Pushes a branch containing the CI configuration
     3. Creates a PR against the GITHUB_REPOSITORY
    """
    # The Github token is implicitly associated with the Github username
    github_api = Github(GITHUB_TOKEN)
    # TODO: Basic validation that the user:
    #  1) Has access to the repo
    #  2) Can write to the repo
    #  3) The token has the repo:write scope
    #     g.oauth_scopes
    #  4) The user isn't rate-limited

    # By default the remote_user_repo will be forked to the API TOKEN
    # user unless GITHUB_USERNAME is specificed to override the user.
    github_user = getenv("GITHUB_USERNAME", github_api.get_user().login)

    if GITHUB_PR_METHOD not in GITHUB_PR_METHODS:
        methods = ", ".join(GITHUB_PR_METHODS)
        sys.exit("GITHUB_PR_METHOD is not one of: %s" % methods)

    github_org = None
    if GITHUB_ORG and (GITHUB_PR_METHOD == PR_REMOTE_ORG_REPO):
        github_org = github_api.get_organization(GITHUB_ORG)

    # The repository need by 'get_repo' should be fully qualifed (ie.
    # include the organization/username)
    repo = github_api.get_repo(GITHUB_REPO)
    default_branch = repo.default_branch

    # Whether forking to a user or an organization, a fork is created
    # with just the repository name:
    #   Fork 'foo/bar' -> USER/bar
    #   Fork 'foo/bar' -> ORG/bar
    # So we rename the repo to include the organization in case there is
    # a name conflict with another organization (ex: org1/foo &
    # org2/foo).
    fork_name = repo.full_name.replace("/", "-")
    # Github repo name limit is 100 chars, so chomp off the end
    if len(fork_name) > 100:
        fork_name = fork_name[:100]

    # Default to local repo for forked repo creation
    fork_url = "https://%s:%s@github.com/%s.git" % (github_user, GITHUB_TOKEN, repo.full_name.rstrip('/'))

    if GITHUB_PR_METHOD != PR_LOCAL_REPO:
        # NOTE: Only PR_LOCAL_REPO will work until this Gitlab issue is
        # resolved (Run CI from a forked Github repo:
        # https://gitlab.com/gitlab-org/gitlab/issues/5667).
        #
        # PR_LOCAL_REPO will push commits to a local branch of the repo
        # being mirrored, and not a fork. This also means the
        # GITHUB_TOKEN user will need write access the the GITHUB_REPO
        # in order to push the branch

        # If a fork doesn't already exist, create it
        if GITHUB_PR_METHOD == PR_REMOTE_ORG_REPO:
            github_fork = github_org.create_fork(repo)
        elif GITHUB_PR_METHOD == PR_REMOTE_USER_REPO:
            github_fork = github_api.get_user().create_fork(repo)

        github_fork.edit(name=fork_name)

        # Because we can't modify the user/pass settings for the forked repo
        # clone_url, we manually recreated in order to do authenticated
        # pushes
        fork_url = "https://%s:%s@github.com/" % (github_user, GITHUB_TOKEN)
        if github_org:
            fork_url += "%s/" % github_org.name
        else:
            fork_url += "%s/" % github_user
        fork_url += "%s.git" % fork_name

    # When running this locally, the fork path may already exist
    fork_path = path.join(TMP_PATH, fork_name)
    if path.exists(fork_path):
        print("%s already exists, deleting" % fork_path)
        shutil.rmtree(fork_path)

    # Clone the forked repository
    Repo.clone_from(fork_url, fork_path)
    fork_repo = Repo(fork_path)
    # Update master branch from upstream.
    # This is needed when multiple PRs are being created over time for the
    # same repo and the previous fork is now out of date from the forked
    # default branch
    # upstream = "https://github.com/%s.git %s" % (GITHUB_REPO, default_branch)
    # fork_repo.git.pull(upstream)
    fork_repo.git.branch(PR_BRANCH)
    fork_repo.git.checkout(PR_BRANCH)

    # Create the commit and push the branch
    copy(PATH, fork_path)
    fork_repo.git.add('--all')
    fork_repo.index.commit(COMMIT_MSG)
    origin = fork_repo.remote(name='origin')
    origin.push(PR_BRANCH)
    # Clean up tmp files after commit has been pushed
    shutil.rmtree(fork_path)

    # Create the PR
    pr_user = github_api.get_user().login
    if github_org:
        pr_user = GITHUB_ORG

    pr_head = PR_BRANCH
    # Append 'USER:' or 'ORG:' to PR head if not coming from a local
    # branch
    if GITHUB_PR_METHOD != PR_LOCAL_REPO:
        pr_head = "{}:{}".format(pr_user, PR_BRANCH)

    pr = repo.create_pull(
        PR_TITLE,
        PR_MSG,
        default_branch,
        pr_head,
        maintainer_can_modify=True
    )
    print("Pull Request has been created at: ", pr.html_url)


if __name__ == "__main__":
    main()
