---
image: circleci/python:3

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

stages:
  - verify
  - toolchain-build
  - toolchain-deploy

before_script:
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate

cache:
  paths:
    - .cache/pip
    - venv

.verify-java-toolchain:
  stage: verify
  except:
    - triggers
    - web
  variables:
    TOOLCHAIN_CI: 'gitlab-ci'
    TOOLCHAIN_LANGUAGE: 'java'
    INCLUDE_DOCUMENTATION: 'n'
  artifacts:
    paths:
      - toolchain_build/
      - toolchain-defaults.yml
  script:
    - pip install cookiecutter
    - >
      tee toolchain-defaults.yml <<EOF >/dev/null

      ---

      default_context:
          ci: ${TOOLCHAIN_CI}
          language: ${TOOLCHAIN_LANGUAGE}
          documentation: ${INCLUDE_DOCUMENTATION}
      EOF
    - cookiecutter --no-input
      --config-file toolchain-defaults.yml
      -o toolchain_build '.'

generate-toolchain:
  only:
    - triggers
    - web
  stage: toolchain-build
  # Default variables have been defined here:
  #   https://gitlab.com/linuxfoundation-it/ci-toolchains/settings/ci_cd
  #   and can be overridden when triggering the job
  artifacts:
    paths:
      - toolchain_build/
      - toolchain-defaults.yml
  script:
    # yamllint disable-line rule:line-length
    - if [ -z $GITHUB_REPOSITORY ]; then echo "Please specify a GITHUB_REPOSITORY"; exit 1; fi
    - echo "Generating $TOOLCHAIN_LANGUAGE CI"
           "for $TOOLCHAIN_CI at $GITHUB_REPOSITORY"
    - pip install cookiecutter
    - >
      tee toolchain-defaults.yml <<EOF >/dev/null

      ---

      default_context:
          ci: ${TOOLCHAIN_CI}
          language: ${TOOLCHAIN_LANGUAGE}
          documentation: ${INCLUDE_DOCUMENTATION}
          build_tool: ${TOOLCHAIN_BUILDTOOL}
          distribution_host: ${DISTRIBUTION_HOST}
          artifact_host: ${ARTIFACT_HOST}
      EOF
    - cookiecutter --no-input
      --config-file toolchain-defaults.yml
      -o toolchain_build '.'

commit-toolchain:
  only:
    - triggers
    - web
  stage: toolchain-deploy
  script:
    - |
        git config --global user.name "ITX CI"
        git config --global user.email "itx@linuxfoundation.org"
    - pip install pygithub gitpython
    - python create-commit.py
      toolchain_build/itx-${TOOLCHAIN_LANGUAGE}-toolchain
