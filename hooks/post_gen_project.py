#!/usr/bin/env python
import os
import shutil

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
ci_platforms_dir = os.path.join(PROJECT_DIRECTORY, 'ci-platforms')

def copy_ci_platforms(provider=None):
    """Add configuration for the selected CI platforms"""
    if provider == "github-actions":
        src = os.path.join(ci_platforms_dir, "github-actions")
        dest = os.path.join(PROJECT_DIRECTORY, os.path.join(".github", "workflows"))
    elif provider == "gitlab-ci":
        src = os.path.join(ci_platforms_dir, "gitlab-ci.yml")
        dest = os.path.join(PROJECT_DIRECTORY, ".gitlab-ci.yml")

    if os.path.isdir(src):
        shutil.copytree(src, dest)
    else:
        shutil.copyfile(src, dest)

def cleanup_ci_platforms(provider=None):
    """Removes all CI configurations that aren't the requested platform
    provider"""
    if provider != "gitlab-ci":
        gitlab_templates = os.path.join(PROJECT_DIRECTORY, '.gitlab-templates')
        shutil.rmtree(gitlab_templates)
    shutil.rmtree(ci_platforms_dir)


def copy_toolchain_files(language=None):
    """Copy toolchain specific files"""
    language_dir = os.path.join(PROJECT_DIRECTORY, '{}-toolchain'.format(language))

    if not os.path.isdir(language_dir):
        return

    # Copy Java .m2 directory
    if language == "java":
        src = os.path.join(language_dir, '.m2')
        dest = os.path.join(PROJECT_DIRECTORY, '.m2')
        shutil.copytree(src, dest)

    shutil.rmtree(language_dir)

def cleanup_toolchain_files(language=None):
    """Remove non-language specific files"""
    if language != 'java':
        java_dir = os.path.join(PROJECT_DIRECTORY, 'java-toolchain')
        shutil.rmtree(java_dir)


def main():
    """Main"""
    provider = '{{ cookiecutter.ci }}'
    language = '{{ cookiecutter.language }}'
    copy_ci_platforms(provider)
    cleanup_ci_platforms(provider)
    copy_toolchain_files(language)
    cleanup_toolchain_files(language)

if __name__ == '__main__':
    main()
