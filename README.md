# CI Toolchains

This is a collection of CI best practices defined through Ansible roles.

[Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/index.html)
is used as the driver for generating the CI configuration.

## Getting Started

Run the following commands and answer the prompts to generate a
toolchain:

```shell
pip3 install cookiecutter
cookiecutter https://github.com/linuxfoundation-it/ci-toolchains
```

You should now have CI configuration specific to the provider and
language you chose. Happy integrating!

## Overview

This project follows two basic steps:

1. Use cookiecutter to generating CI toolchain
2. Push the toolchain as pull request (PR) against a Github repository

The script to create PRs currently requires write access to the
repository where the CI configuration should be committed.

A set of [sample project repositories](https://github.com/linuxfoundation-it?utf8=✓&q=ci-sample)
has been created under the linuxfoundation-it organization for testing
the toolchains.

## Remotely Generate a Toolchain

> Note: The lf-itx user will need write access to the Github repository
> the toolchain is being submitted to.

A toolchain can be remotely generated and submitted with the following
cURL call and a
[trigger token](https://gitlab.com/help/ci/triggers/README#authentication-tokens):

```shell
#!/bin/bash
GITLAB_TOKEN=<GITLAB_TOKEN>

curl -X POST \
     -F token=$GITLAB_TOKEN \
     -F "ref=master" \
     -F "variables[GITHUB_REPOSITORY]=linuxfoundation-it/ci-sample-java-maven" \
     -F "variables[TOOLCHAIN_LANGUAGE]=java" \
     -F "variables[INCLUDE_DOCUMENTATION]=n" \
     https://gitlab.com/api/v4/projects/12507587/trigger/pipeline
```

Then you should see the job run on the [repository](https://gitlab.com/linuxfoundation-it/ci-toolchains/pipelines)

## Contributing

### Developing Toolchains

1. Install Cookiecutter
    ```shell
    pip3 install cookiecutter
    ```

1. Clone the repository

    ```shell
    git clone https://github.com/linuxfoundation-it/ci-toolchains
    ```

1. Clone the sample project code the toolchain will be ran against (ex:
   Java+Maven)

   ```shell
   git clone https://github.com/linuxfoundation-it/ci-sample-java-maven
   ```

1. Modify your files (CI configuration, playbooks, roles,
   cookiecutter.json, etc.)

1. Run cookiecutter to verify the Jinja2 syntax is correct and hooks
   work as intended:

   ```shell
   $ cat defaults.yml
   ---
   default_context:
       ci: 'gitlab-ci'
       language: 'java'
       documentation: 'n'
   ```

   ```shell
   cookiecutter --no-input --config-file defaults.yml .
   ```

1. Check the toolchain directory looks right:

   ```shell
   tree -a itx-<TOOLCHAIN>-toolchain
   ```

1. Copy the toolchain files into the sample repository for testing

   ```shell
   cp -r itx-java-toolchain/* ci-sample-java-maven/
   ```

1. Run the toolchain locally against the repository to verify your jobs
   work as intended. Specific instructions on running the CI locally can
   be found in the .commit-msg file of the generated toolchain
   directory.

1. Repeat the process till jobs are in the state you want, then commit
   the code back (under ci-toolchains).

### Developing the PR

The `create-commit.py` script handles creating the git commit and
publishing a PR. It does this all through the Github API.

1. Generate your toolchain

1. Run the create-commit.py script:

   ```shell
   GITHUB_TOKEN=<MY_GITHUB_TOKEN> \
   GITHUB_REPOSITORY=<GITHUB_REPOSITORY_NEEDING_CI> \
   python create-commit.py itx-<TOOLCHAIN>-toolchain
   ```

1. View the PR to ensure the commit message, files, etc. are all
   correct.
